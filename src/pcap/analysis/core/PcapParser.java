package pcap.analysis.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pcap.analysis.utils.Utilities;

/**
 * Reference :
 * http://www.kroosec.com/2012/10/a-look-at-pcap-file-format.html
 * https://code.google.com/p/capture-analyzer/
 * 
 * Pcap File format
 * | File Header (24) | Packet Header (16) | Packet Data | Packet Header (16) | Packet Data | ... ||||
 * 
 * Steps in parsing the pcap file :
 * 1. Remove the 24 bytes of file header. TODO Magic Number parsing & Validations. 
 * 	  Assumptions : Little E, Max packet data size - 65535 (0xffff0000), Ethernet Link layer protocol
 * 2. Remove the packet header (16 bytes) and extract the length of the packet as it was captured on 
 * 	  the wire (4th field - last 4 bytes) from the header
 * 3. Extract the packet data based on the above obtained length
 * 4. Populate the list and goto Step 2 till end of stream
 * 
 * @author prajithr
 * 
 */


public class PcapParser {

	private String pcapFileName;
		
	public PcapParser(String pcapFileName)
	{
		this.pcapFileName = pcapFileName;
	}

	/**
	 * Reference :
	 * http://www.kroosec.com/2012/10/a-look-at-pcap-file-format.html
	 * https://code.google.com/p/capture-analyzer/
	 * 
	 * Pcap File format
	 * | File Header (24) | Packet Header (16) | Packet Data | Packet Header (16) | Packet Data | ... ||||
	 * 
	 * Steps in parsing the pcap file :
	 * 1. Remove the 24 bytes of file header. TODO Magic Number parsing & Validations. 
	 * 	  Assumptions : Little E, Max packet data size - 65535 (0xffff0000), Ethernet Link layer protocol
	 * 2. Remove the packet header (16 bytes) and extract the length of the packet as it was captured on 
	 * 	  the wire (4th field - last 4 bytes) from the header
	 * 3. Extract the packet data based on the above obtained length
	 * 4. Populate the list and goto Step 2 till end of stream 
	 * 
	 * @param pcapFileName
	 * @return List of packet data in pcap file
	 */
	public List<byte[]> getPacketDataList ()
	{
		InputStream inPcapStream = null;
		
		try {
			
			inPcapStream = new FileInputStream(pcapFileName);
			
			// Step 1. Remove the 24 bytes of file header. 
			getFileHeader(inPcapStream);
			
			List<byte[]> packetDataList = new ArrayList<>();
			byte[] packetData = null;
			
			//Step 4. Populate the list and goto Step 2 till end of stream
			while ((packetData = getPacketData(inPcapStream)) != null)
			{
				packetDataList.add(packetData);
			}

			return packetDataList;
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try {
				inPcapStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	/**
	 * Takes the pcap file header (first 24 bytes) removed stream and 
	 * returns the first raw packet data in the file 
	 * 
	 * @param inPcapStream
	 * @return
	 * @throws IOException 
	 */
	private byte[] getPacketData(InputStream inPcapStream) throws IOException {

		// Step 2. Remove the packet header (16 bytes) and extract the length of the packet as it was captured on 
		// the wire (4th field - last 4 bytes) from the header
		Long packetDataLength = getPacketDataLength(inPcapStream);

		if (packetDataLength == null)
			return null;

		byte[] packetData = new byte[packetDataLength.intValue()];

		// Step 3. Extract the packet data based on the above obtained length
		if (inPcapStream.read(packetData) != packetData.length)
		{
			throw new IOException("Problem in file");
		}

		return packetData;

	}

	/**
	 * 
	 * @param inPcapStream
	 * @throws IOException 
	 */
	private Long getPacketDataLength(InputStream inPcapStream) throws IOException {
		
		byte[] pcapPacketHeader = new byte[16];
		if (inPcapStream.read(pcapPacketHeader) != pcapPacketHeader.length) 
			return null;
        
		// TODO Little E is assumed. 
		// Extract the length of the packet as it was captured on 
		// the wire (4th field - last 4 bytes) from the header and then flip
		
		return Utilities.convert32LittleE2BigE(Utilities.extractBigE32(pcapPacketHeader, 12));
        
	}

	/**
	 * Read first 24 bytes from pcap file to get pcap header
	 * @param inPcapStream
	 * @throws IOException 
	 */
	private byte[] getFileHeader(InputStream inPcapStream) throws IOException {
		 byte[] header = new byte[24];
		 inPcapStream.read(header);
		 return header;
	}
	
	
	
}
	