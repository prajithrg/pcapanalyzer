package pcap.analysis.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import pcap.analysis.utils.Utilities;

/**
 * DestinationPortDistribution class which has the main method.
 * Uses the PcapParser to get the packet data list and then 
 * uses the Utility class to create the map of destination port 
 * and the count. Prints the map on to standard out in descending 
 * order of the count.
 * 
 * @author prajithr
 */

public class DestinationPortDistribution {

	private static Map<Integer, Integer> dstPortCountMap = new HashMap<Integer, Integer>();

	public static void main(String[] args) {

		PcapParser parser = new PcapParser(args[0]);

		// Create list containing all the raw packet data from the pcap file
		List<byte[]> packetDataList = parser.getPacketDataList();

		for (byte[] packetData :  packetDataList)
		{
			int dstPortNo = Utilities.getDestinationPort(packetData);

			if (dstPortNo != 0 )
			{
				int currentCount;
				if (dstPortCountMap.containsKey(dstPortNo))
				{
					currentCount = dstPortCountMap.get(dstPortNo);
					dstPortCountMap.put(dstPortNo, currentCount+1);
				}
				else 
					dstPortCountMap.put(dstPortNo, 1);
			}
		}


		// Sort the DstPort-Count Map in descending order of count and print the result
		Set<Entry<Integer, Integer>> set = dstPortCountMap.entrySet();
		List<Entry<Integer, Integer>> list = new ArrayList<Entry<Integer, Integer>>(set);
		Collections.sort( list, new Comparator<Entry<Integer, Integer>>()
				{
					public int compare( Entry<Integer, Integer> o1, Entry<Integer, Integer> o2 )
						{
							return (o2.getValue()).compareTo( o1.getValue() );
						}
				});
		
		for(Entry<Integer, Integer> entry : list){
			System.out.println("DstPort : " + entry.getKey() + " , Count : " + entry.getValue());
		}

	}

}
