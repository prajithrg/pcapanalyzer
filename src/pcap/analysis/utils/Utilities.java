package pcap.analysis.utils;

/**
 * Utility class for performing byte operations and 
 * packet field checking
 * 
 * @author prajithr
 */

public class Utilities {

	//Ethernet
	private static final int ETHERNET_TYPE_OFFSET = 12;
	private static final int ETHERNET_IP_PACKET_TYPE = 0x800;
	private static final int ETHERNET_HEADER_LENGTH = 14;
	private static final int IP_PROTOCOL_LOCATION = 9; 
	
	//TCP
	private static final int TCP = 6;
	private static final int TCP_DST_PORT_LOCATION = 2;

	//UDP
	private static final int UDP = 0x11;
	private static final int UDP_DST_PORT_LOCATION = 2;


	/**
	 * Extract 32 bits in BigE order from index
	 * 
	 * @param packetHeader
	 * @param index
	 * @return extracted no 
	 */

	public static Long extractBigE32 (byte[] packetHeader, int index)
	{
		long field = 0;
		for (int i=0 ; i<4 ; i++)
		{
			field = (field<<8) + ( packetHeader[i + index] & 0xff );
		}
		return field;

	}

	/**
	 * Extract 16 bits in BigE order from index
	 * 
	 * @param bytes
	 * @param index 
	 * @return extracted no
	 */
	public static int extractBigE16 (byte[] bytes, int index)
	{
		int field = 0;
		for (int i=0 ; i<2 ; i++)
		{
			field = (field<<8) + (bytes[i + index] & 0xff );
		}
		return field;
	}

	/**
	 * Extract 8 bits in BigE order from index
	 * 
	 * @param bytes
	 * @param index 
	 * @return extracted no
	 */
	public static int extractBigE8 (byte[] bytes, int index)
	{
		return bytes[index] & 0xff;
	}

	/**
	 * Extracts the destination port if its an IP packet and then the 
	 * ipType is either TCP/ UDP
	 * 
	 * @param packetData
	 */
	public static int getDestinationPort (byte[] packetData)
	{
		if (isIpPacket(packetData))
		{
			int ipType = getIpProtocolType(packetData);
			if (ipType == TCP)
			{
				return getTCPDestinationPort(packetData);
			}
			else if (ipType == UDP)
			{
				return getUDPDestinationPort(packetData);
			}
		}
		return 0;
	}

	/**
	 * @param packet
	 * @return UDP Destination port
	 */
	private static int getUDPDestinationPort(byte[] packet) 
	{
		int ipHeaderOffset = ETHERNET_HEADER_LENGTH;
		int ipHeaderLength = ( packet[ipHeaderOffset] & (0x0f) ) * 4;
		int udpHeaderOffset = ipHeaderOffset + ipHeaderLength;
		return  extractBigE16(packet, udpHeaderOffset + UDP_DST_PORT_LOCATION);
	}

	/**
	 * @param packet
	 * @return true if IP 
	 */
	private static boolean isIpPacket(byte[] packet) {

		return  (extractBigE16(packet, ETHERNET_TYPE_OFFSET) == ETHERNET_IP_PACKET_TYPE);
	}


	/**
	 * Extracts the IP protocol type from the IP header
	 * 
	 * @param packetData
	 * @return IP Type 
	 */
	private static int getIpProtocolType(byte[] packetData)
	{
		return extractBigE8(packetData, ETHERNET_HEADER_LENGTH + IP_PROTOCOL_LOCATION);
	}

	/**
	 * @param packetData 
	 * @return the TCP destination port
	 */
	private static int getTCPDestinationPort(byte[] packetData)
	{
		int ipHeaderOffset = ETHERNET_HEADER_LENGTH;
		int ipHeaderLength = ( packetData[ipHeaderOffset] & (0x0f) ) * 4;
		int tcpHeaderOffset = ipHeaderOffset + ipHeaderLength;

		return  extractBigE16(packetData, tcpHeaderOffset + TCP_DST_PORT_LOCATION);

	}

	/**
	 * Convert 32 Little E to Big E and vice versa
	 * @param number
	 * @return 
	 */
	public static long convert32LittleE2BigE(long number)
	{
		long convert = number;
		convert = ((convert & 0xFF000000) >> 24) + ((convert & 0x00FF0000) >> 8) + ((convert & 0x0000FF00) << 8) + ((convert & 0x000000FF) << 24);
		
		return convert;
	}

}
