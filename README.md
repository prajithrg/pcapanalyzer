Used to parse the pcap file and get count distribution of destination ports in the packets.

The runnable jar is built using JDK 1.7.

Usage : 
        java -jar PcapDstPortDistribution.jar <absolue path to the pcap file>

        Example > java -jar PcapDstPortDistribution.jar /home/prajithrg/vboxshared/slowdownload.pcap

 Reference :
	  http://www.kroosec.com/2012/10/a-look-at-pcap-file-format.html
	  https://code.google.com/p/capture-analyzer/